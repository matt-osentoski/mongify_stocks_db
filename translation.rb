table "stock_prices" do
	column "id", :key, :as => :integer
	column "symbol", :string
	column "price_date", :timestamps
	column "opening_price", :decimal
	column "high_price", :decimal
	column "low_price", :decimal
	column "closing_price", :decimal
	column "adj_closing_price", :decimal
	column "period", :string
	column "volume", :integer
  before_save do |row|
    row = (row.id > 0 && row.id < 5000) ? row : nil # Grab the first 5000 rows
  end
end

#table "stocks" do
#	column "id", :key, :as => :integer
#	column "symbol", :string
#	column "name", :string
#	column "description", :text
#	column "sector", :string
#	column "industry", :string
#	column "stock_index", :string
#	column "is_sp500", :string
#	column "is_sp1500", :string
#	column "is_dow_ind", :string
#end

#table "treasuries" do
#	column "id", :key, :as => :integer
#	column "time_period", :timestamps
#	column "1_month", :decimal
#	column "3_month", :decimal
#	column "6_month", :decimal
#	column "1_year", :decimal
#	column "2_year", :decimal
#	column "3_year", :decimal
#	column "5_year", :decimal
#	column "7_year", :decimal
#	column "10_year", :decimal
#	column "20_year", :decimal
#	column "30_year", :decimal
#end

