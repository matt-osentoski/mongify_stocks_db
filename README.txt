# This project uses the Ruby gem mongify to convert MySQL tables into a MongoDB database.
(Ref:  http://mongify.com/getting_started.html)

1. install mongify
gem install mongify

2. Update the following file to match your MySQL and MongoDb databases:
database.config

3. Run the following command from the directory where 'database.config' lives:
mongify check database.config

4. Run the following command to build the translation file
mongify translation database.config > translation.rb

5. Comment out the 'stock_prices' table in the translation.rb file.  This table will be too large to process in memory. Also, change DATETIME types to TIMESTAMP

6. Run the following command to perform the ETL process from MySQL to MongoDB
mongify process database.config translation.rb 


(NOTE: The stock_prices table is TOO large to use this program. You will have to manually create CSV files from MySQL using the following syntax:)

-- The union adds a header row at the top of the file

SELECT 'id', 'symbol', 'price_date', 'opening_price', 'high_price', 'low_price',
'closing_price', 'adj_closing_price', 'period', 'volume'
UNION
SELECT id, symbol, price_date, opening_price, high_price, low_price,
closing_price, adj_closing_price, period, volume FROM stock_prices
WHERE price_date BETWEEN "1980-01-01" and "1985-12-31"
INTO OUTFILE 'c:/tmp/stock_prices_1980_1985.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'


--Then import into mongoDB

mongoimport -d stocks_10202013 -c stock_prices --type csv --file stock_prices_1980_1985.csv --headerline


-- Create some indexes
db.stocks.ensureIndex( {symbol: 1 });
db.stock_prices.ensureIndex( {symbol: 1, period: 1});


-- Next you will need to create random attributes in the documents for stocks and stock_prices
-- Perform the following commands from the mongoDb command prompt:
function setRandom() {
	db.stocks.find().forEach(function (obj) {
		obj.random = Math.random();
		db.stocks.save(obj);
	}); 
} 
db.eval(setRandom);
db.stocks.ensureIndex( { random: 1 } );

function setRandom() {
	db.stock_prices.find().forEach(function (obj) {
		obj.random = Math.random();
		db.stock_prices.save(obj);
	}); 
} 
db.eval(setRandom);
db.stock_prices.ensureIndex( { symbol: 1, period: 1, random: 1 } );


-- Next convert the dates to ISO standard formatted dates:
function convertToISODates() {
	db.stock_prices.find().forEach(function (obj) {
		obj.price_date = new Date(obj.price_date);
		db.stock_prices.save(obj);
	}); 
} 
db.eval(convertToISODates);
db.stock_prices.ensureIndex( { symbol: 1, period: 1, price_date: 1 } );

-- Finally, perform a dump of the database:
mongodump --out c:\data\backup --db stocks_10202013
